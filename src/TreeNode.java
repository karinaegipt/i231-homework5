import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   // Konstruktor
   TreeNode (String n, TreeNode d, TreeNode r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   public static TreeNode parsePrefix (String s) {
      s = s.trim();
      if (s.isEmpty() || s.contains(" ")) {
         throw  new RuntimeException("Sisend '" + s + "' on vigane");
      }

      int openBracketCount = 0; // avatud sulgude arv
      int closeBracketCount = 0; // suletud sulgude arv.

      Matcher m = Pattern.compile("\\(").matcher(s);
      while (m.find()) openBracketCount++;

      m = Pattern.compile("\\)").matcher(s);
      while (m.find()) closeBracketCount++;

      //Konytollin kas kõik avatud sulud ka suletakse
      if (openBracketCount != closeBracketCount ||
              (openBracketCount > 0 && s.charAt(s.length() - 1) != ')') ||
              (s.contains(",") && openBracketCount == 0)) {
         throw  new RuntimeException("Sisend '" + s + "' on vigane. Sulgude arv ei klapi.");
      }
      return parseNode(s.trim(), 0, false).value;
   } //parsePrefix

   static class ParseResult {
      int newIndex;
      TreeNode value;
      boolean bracketEnd;
   }
   // Sisendi parsimine
   public static ParseResult parseNode(String s, int parseOffset, boolean expectEnd) {
      final TreeNode firstNode = new TreeNode("", null, null);
      TreeNode node = firstNode;
      ParseResult tmpResult = null;

      loop: for (int i = parseOffset; i < s.length(); i++) {
         switch (s.charAt(i)) {
            case '(':
               tmpResult = parseNode(s, i + 1, true);
               i = tmpResult.newIndex;
               firstNode.firstChild = tmpResult.value;
               break;
            case ',':
               tmpResult = parseNode(s, i + 1, true);
               i = tmpResult.newIndex - 1;
               firstNode.nextSibling = tmpResult.value;
               break;
            default:
               if (s.charAt(i) != ')') {
                  node.name += s.charAt(i);
               }

               if (s.charAt(i) == ')' || i == s.length() - 1) {
                  if (s.charAt(i) == ')' &&
                       (!expectEnd || firstNode.name.isEmpty() )) {
                     throw new RuntimeException("Sisend '" + s + "' on vigane. Sulg on puudu.");
                  }
                  ParseResult result = new ParseResult();
                  result.newIndex = i;
                  result.value = firstNode;

                  return result;
               }
         }
      }

      if (parseOffset != 0) {
         throw new RuntimeException("Sisend '" + s + "' on vigane.");
      }

      return new ParseResult(){{ value = firstNode; }};
   }

   public String rightParentheticRepresentation() {
      return composeRPR(this);
   }

   // Stringi ümberpööramiseks
   public static String composeRPR (TreeNode node)  {
      List<String>  list = new ArrayList<String>();
      TreeNode currentNode = node;

      do {
         String tmp = "";
         if (currentNode.firstChild != null) {
            tmp += "(" + composeRPR(currentNode.firstChild) + ")";
         }
         list.add(tmp + currentNode.name);
         currentNode = currentNode.nextSibling;
      } while (currentNode != null);

      return String.join(",", list);
   }

   public static void main (String[] param) {
      String s = "A(B1,C,D)";
      TreeNode t = TreeNode.parsePrefix (s);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}

